// ======= LAST 30 DAYS MOST PLAYED TRACKS =========

$(document).ready(function() {
       $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.gettoptracks&api_key=7048860945c242165adb91eac98565b3&user=runawayz&period=1month&format=json&limit=50&page=1", function(json) {
           var topTracks = '';
           $.each(json.toptracks.track, function(i, item) {
               topTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i+1) + ". " + "</b></td><td width=\"400px\">" + item.artist.name + "</td><td width=\"400px\">" +  item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
           });
           $('#last30Days').append(topTracks);
       });
   });


// ======= 2018 TOP 100 =========

 $(document).ready(function() {
       $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1514757600&to=1546293599&limit=100&page=1", function(json) {
           var thisYearTopTracks = '';
           $.each(json.weeklytrackchart.track, function(i, item) {
                if (i<100){
                    return thisYearTopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
                    }
           });
           $('#year2018').append(thisYearTopTracks);
       });
   });

 // ======2018 TOP ARTISTS ======


 $(document).ready(function() {
     $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1514757600&to=1546293599&limit=10", function(json) {
         var topArtists2018 = '';
         $.each(json.weeklyartistchart.artist, function(i, item) {
             if (i<10){
                 return topArtists2018 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

             }
         });
         $('#year2018Artists').append(topArtists2018);
     });
 });

 // ====== 2018 TOP ALBUMS ========

 $(document).ready(function() {
     $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1514757600&to=1546293599&limit=10", function(json) {
         var topAlbums2018 = '';
         $.each(json.weeklyalbumchart.album, function(i, item) {
             if (i<10){
                 return topAlbums2018 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

             }
         });
         $('#year2018Albums').append(topAlbums2018);
     });
 });

// ========== 2017 TOP 100 =============

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1483221600&to=1514757599&limit=100&page=1", function(json) {
        var year2017TopTracks = '';
        $.each(json.weeklytrackchart.track, function(i, item) {
            if (i<100){
                return year2017TopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
            }
        });
        $('#year2017').append(year2017TopTracks);
    });
});


// ======2017 TOP ARTISTS ======


$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1483221600&to=1514757599&limit=10", function(json) {
        var topArtists2017 = '';
        $.each(json.weeklyartistchart.artist, function(i, item) {
            if (i<10){
                return topArtists2017 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

            }
        });
        $('#year2017Artists').append(topArtists2017);
    });
});

// ====== 2017 TOP ALBUMS ========

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1483221600&to=1514757599&limit=10", function(json) {
        var topAlbums2017 = '';
        $.each(json.weeklyalbumchart.album, function(i, item) {
            if (i<10){
                return topAlbums2017 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

            }
        });
        $('#year2017Albums').append(topAlbums2017);
    });
});

// ========== 2016 TOP 100 =============

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1451599200&to=1483221599&limit=100&page=1", function(json) {
        var year2016TopTracks = '';
        $.each(json.weeklytrackchart.track, function(i, item) {
            if (i<100){
                return year2016TopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
            }
        });
        $('#year2016').append(year2016TopTracks);
    });
});


// ======2016 TOP ARTISTS ======


$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1451599200&to=1483221599&limit=10", function(json) {
        var topArtists2016 = '';
        $.each(json.weeklyartistchart.artist, function(i, item) {
            if (i<10){
                return topArtists2016 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

            }
        });
        $('#year2016Artists').append(topArtists2016);
    });
});

// ====== 2016 TOP ALBUMS ========

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1451599200&to=1483221599&limit=10", function(json) {
        var topAlbums2016 = '';
        $.each(json.weeklyalbumchart.album, function(i, item) {
            if (i<10){
                return topAlbums2016 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

            }
        });
        $('#year2016Albums').append(topAlbums2016);
    });
});

// ========== 2015 TOP 100 =============

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1420063200&to=1451599199&limit=100&page=1", function(json) {
        var year2015TopTracks = '';
        $.each(json.weeklytrackchart.track, function(i, item) {
            if (i<100){
                return year2015TopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
            }
        });
        $('#year2015').append(year2015TopTracks);
    });
});


// ======2015 TOP ARTISTS ======


$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1420063200&to=1451599199&limit=10", function(json) {
        var topArtists2015 = '';
        $.each(json.weeklyartistchart.artist, function(i, item) {
            if (i<10){
                return topArtists2015 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

            }
        });
        $('#year2015Artists').append(topArtists2015);
    });
});

// ====== 2015 TOP ALBUMS ========

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1420063200&to=1451599199&limit=10", function(json) {
        var topAlbums2015 = '';
        $.each(json.weeklyalbumchart.album, function(i, item) {
            if (i<10){
                return topAlbums2015 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

            }
        });
        $('#year2015Albums').append(topAlbums2015);
    });
});

// ========== 2014 TOP 100 =============

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1388527200&to=1420063199&limit=100&page=1", function(json) {
        var year2014TopTracks = '';
        $.each(json.weeklytrackchart.track, function(i, item) {
            if (i<100){
                return year2014TopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
            }
        });
        $('#year2014').append(year2014TopTracks);
    });
});


// ======2014 TOP ARTISTS ======


$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1388527200&to=1420063199&limit=10", function(json) {
        var topArtists2014 = '';
        $.each(json.weeklyartistchart.artist, function(i, item) {
            if (i<10){
                return topArtists2014 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

            }
        });
        $('#year2014Artists').append(topArtists2014);
    });
});

// ====== 2014 TOP ALBUMS ========

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=runawayz&api_key=7048860945c242165adb91eac98565b3&format=json&from=1388527200&to=1420063199&limit=10", function(json) {
        var topAlbums2014 = '';
        $.each(json.weeklyalbumchart.album, function(i, item) {
            if (i<10){
                return topAlbums2014 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

            }
        });
        $('#year2014Albums').append(topAlbums2014);
    });
});

// ========== 2013 TOP 100 =============

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1356991200&to=1388527199&limit=100&page=1", function(json) {
        var year2013TopTracks = '';
        $.each(json.weeklytrackchart.track, function(i, item) {
            if (i<100){
                return year2013TopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
            }
        });
        $('#year2013').append(year2013TopTracks);
    });
});


// ======2013 TOP ARTISTS ======


$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1356991200&to=1388527199&limit=10", function(json) {
        var topArtists2013 = '';
        $.each(json.weeklyartistchart.artist, function(i, item) {
            if (i<10){
                return topArtists2013 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

            }
        });
        $('#year2013Artists').append(topArtists2013);
    });
});

// ====== 2013 TOP ALBUMS ========

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1356991200&to=1388527199&limit=10", function(json) {
        var topAlbums2013 = '';
        $.each(json.weeklyalbumchart.album, function(i, item) {
            if (i<10){
                return topAlbums2013 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

            }
        });
        $('#year2013Albums').append(topAlbums2013);
    });
});

// ========== 2012 TOP 100 =============

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1325368800&to=1356991199&limit=100&page=1", function(json) {
        var year2012TopTracks = '';
        $.each(json.weeklytrackchart.track, function(i, item) {
            if (i<100){
                return year2012TopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
            }
        });
        $('#year2012').append(year2012TopTracks);
    });
});


// ======2012 TOP ARTISTS ======


$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1325368800&to=1356991199&limit=10", function(json) {
        var topArtists2012 = '';
        $.each(json.weeklyartistchart.artist, function(i, item) {
            if (i<10){
                return topArtists2012 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

            }
        });
        $('#year2012Artists').append(topArtists2012);
    });
});

// ====== 2012 TOP ALBUMS ========

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1325368800&to=1356991199&limit=10", function(json) {
        var topAlbums2012 = '';
        $.each(json.weeklyalbumchart.album, function(i, item) {
            if (i<10){
                return topAlbums2012 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

            }
        });
        $('#year2012Albums').append(topAlbums2012);
    });
});

// ========== 2011 TOP 100 =============

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1293832800&to=1325368799&limit=100&page=1", function(json) {
        var year2011TopTracks = '';
        $.each(json.weeklytrackchart.track, function(i, item) {
            if (i<100){
                return year2011TopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
            }
        });
        $('#year2011').append(year2011TopTracks);
    });
});


// ======2011 TOP ARTISTS ======


$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1293832800&to=1325368799&limit=10", function(json) {
        var topArtists2011 = '';
        $.each(json.weeklyartistchart.artist, function(i, item) {
            if (i<10){
                return topArtists2011 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

            }
        });
        $('#year2011Artists').append(topArtists2011);
    });
});

// ====== 2011 TOP ALBUMS ========

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1293832800&to=1325368799&limit=10", function(json) {
        var topAlbums2011 = '';
        $.each(json.weeklyalbumchart.album, function(i, item) {
            if (i<10){
                return topAlbums2011 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

            }
        });
        $('#year2011Albums').append(topAlbums2011);
    });
});

// ========== 2010 TOP 100 =============

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1262296800&to=1293832799&limit=100&page=1", function(json) {
        var year2010TopTracks = '';
        $.each(json.weeklytrackchart.track, function(i, item) {
            if (i<100){
                return year2010TopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
            }
        });
        $('#year2010').append(year2010TopTracks);
    });
});


// ======2010 TOP ARTISTS ======


$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1262296800&to=1293832799&limit=10", function(json) {
        var topArtists2010 = '';
        $.each(json.weeklyartistchart.artist, function(i, item) {
            if (i<10){
                return topArtists2010 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

            }
        });
        $('#year2010Artists').append(topArtists2010);
    });
});

// ====== 2010 TOP ALBUMS ========

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1262296800&to=1293832799&limit=10", function(json) {
        var topAlbums2010 = '';
        $.each(json.weeklyalbumchart.album, function(i, item) {
            if (i<10){
                return topAlbums2010 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

            }
        });
        $('#year2010Albums').append(topAlbums2010);
    });
});

// ========== 2009 TOP 100 =============

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1230760800&to=1262296799&limit=100&page=1", function(json) {
        var year2009TopTracks = '';
        $.each(json.weeklytrackchart.track, function(i, item) {
            if (i<100){
                return year2009TopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
            }
        });
        $('#year2009').append(year2009TopTracks);
    });
});


// ======2009 TOP ARTISTS ======


$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1230760800&to=1262296799&limit=10", function(json) {
        var topArtists2009 = '';
        $.each(json.weeklyartistchart.artist, function(i, item) {
            if (i<10){
                return topArtists2009 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

            }
        });
        $('#year2009Artists').append(topArtists2009);
    });
});

// ====== 2009 TOP ALBUMS ========

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1230760800&to=1262296799&limit=10", function(json) {
        var topAlbums2009 = '';
        $.each(json.weeklyalbumchart.album, function(i, item) {
            if (i<10){
                return topAlbums2009 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

            }
        });
        $('#year2009Albums').append(topAlbums2009);
    });
});

// ========== 2008 TOP 100 =============

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklytrackchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1199138400&to=1230760799&limit=100&page=1", function(json) {
        var year2008TopTracks = '';
        $.each(json.weeklytrackchart.track, function(i, item) {
            if (i<100){
                return year2008TopTracks += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"350px\">" + item.artist['#text'] + "</td><td width=\"350px\">" + item.name + "</td><td align='center'>" + "<a href=" + item.url + " target='_blank'>" + "<b>" + item.playcount + "</a></b></td></tr>";
            }
        });
        $('#year2008').append(year2008TopTracks);
    });
});


// ======2008 TOP ARTISTS ======


$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyartistchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1199138400&to=1230760799&limit=10", function(json) {
        var topArtists2008 = '';
        $.each(json.weeklyartistchart.artist, function(i, item) {
            if (i<10){
                return topArtists2008 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"250px\">" + item.name + "</td></tr>";

            }
        });
        $('#year2008Artists').append(topArtists2008);
    });
});

// ====== 2008 TOP ALBUMS ========

$(document).ready(function() {
    $.getJSON("http://ws.audioscrobbler.com/2.0/?method=user.getweeklyalbumchart&user=poseidons&api_key=7048860945c242165adb91eac98565b3&format=json&from=1199138400&to=1230760799&limit=10", function(json) {
        var topAlbums2008 = '';
        $.each(json.weeklyalbumchart.album, function(i, item) {
            if (i<10){
                return topAlbums2008 += "<tr><td align='center', td width=\"50px\"><b>" + (i + 1) + ". " + "</b></td><td width=\"400px\">" + item.artist['#text'] + " - "  + item.name + "</td></tr>";

            }
        });
        $('#year2008Albums').append(topAlbums2008);
    });
});


// =========== FUNCTIONS =============

// ======== to open tabs on click ==========

 function openTab(evt, tabName) {
         var i, tabcontent, tablinks;
         tabcontent = document.getElementsByClassName("tabcontent");
         for (i = 0; i < tabcontent.length; i++) {
             tabcontent[i].style.display = "none";
         }
         tablinks = document.getElementsByClassName("tablinks");

         for (i = 0; i < tablinks.length; i++) {
             tablinks[i].className = tablinks[i].className.replace(" active", "");
         }
         document.getElementById(tabName).style.display = "block";
         evt.currentTarget.className += " active";


 }

$(function(){
    $("#2017FinalChart").load("2017 - TOP 100.mht");
});

function loading() {
    var bar = new ProgressBar.Circle(container, {
        color: '#aaa',
        // This has to be the same size as the maximum width to
        // prevent clipping
        strokeWidth: 4,
        trailWidth: 1,
        easing: 'easeInOut',
        duration: 1400,
        text: {
            autoStyleContainer: false
        },
        from: { color: '#aaa', width: 1 },
        to: { color: '#333', width: 4 },
        // Set default step function for all animate calls
        step: function(state, circle) {
            circle.path.setAttribute('stroke', state.color);
            circle.path.setAttribute('stroke-width', state.width);

            var value = Math.round(circle.value() * 100);
            if (value === 0) {
                circle.setText('');
            } else {
                circle.setText(value);
            }

        }
    });
    bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
    bar.text.style.fontSize = '2rem';

    bar.animate(1.0);  // Number from 0.0 to 1.0
}